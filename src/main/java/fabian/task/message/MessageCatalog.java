package fabian.task.message;

import java.util.*;

public class MessageCatalog {

    private Collection<Message> messages = new ArrayList<>();

    public void addMessage(Message message){
        messages.add(message);
    }

    public void removeMessage(Message message){
        messages.remove(message);
    }

    public Collection<Message> getMessages() {
        return messages;
    }
}
