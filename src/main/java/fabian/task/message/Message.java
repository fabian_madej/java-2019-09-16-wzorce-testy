package fabian.task.message;

import java.time.LocalDate;

public class Message {

    private String title;
    private String author;
    private String contents;
    private LocalDate creation_date;

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public String getContents() {
        return contents;
    }

    public LocalDate getCreation_date() {
        return creation_date;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }

    public void setCreation_date(LocalDate creation_date) {
        this.creation_date = creation_date;
    }

    public String  toString(){
        return "Title: " + title + "\nAuthor: " + author +
                "\nDate: " + creation_date + "\nContent: " + contents + "\n";
    }

    public String toSimpleString() {
        return "Title: " + title + "\nAuthor: " + author + "\n";
    }
}
