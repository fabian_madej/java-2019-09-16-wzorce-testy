package fabian.task.message;

import java.time.LocalDate;
import java.util.Date;

public class MessageBuilder {

    private String title;
    private String author;
    private String contents;
    private LocalDate creation_date;

    public MessageBuilder withTitle(String title){
        this.title=title;
        return this;
    }

    public MessageBuilder withAuthor(String author){
        this.author=author;
        return this;
    }

    public MessageBuilder withContents(String contents){
        this.contents=contents;
        return this;
    }

    public MessageBuilder withCreation_date(LocalDate creation_date){
        this.creation_date=creation_date;
        return this;
    }

    public Message build(){
        Message m = new Message();
        m.setTitle(this.title);
        m.setAuthor(this.author);
        m.setContents(this.contents);
        m.setCreation_date(this.creation_date);
        return m;
    }

}
