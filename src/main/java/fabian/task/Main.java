package fabian.task;

import fabian.task.messageManager.MessageManager;

public class Main {
    public static void main(String[] args) {

        MessageManager messageManager = new MessageManager();

        messageManager.start();

    }
}
