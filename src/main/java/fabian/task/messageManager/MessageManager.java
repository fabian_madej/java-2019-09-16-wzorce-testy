package fabian.task.messageManager;

import fabian.task.message.MessageCatalog;

import java.util.Scanner;

public class MessageManager {

    public void start() {

        Scanner scanner = new Scanner(System.in);

        MessageCatalog messageCatalog = new MessageCatalog();
        MessageDeleteManager messageOptions = new MessageDeleteManager();
        ShowMessage showMessage = new ShowMessage();
        FilterMessage filterMessage = new FilterMessage();
        AddMessage addMessage = new AddMessage();
        ManagerMenu managerMenu = new ManagerMenu();

        System.out.println("Welcome, what do you want to do?");

        boolean end = true;
        do{

            System.out.println("Write 'menu' if you want to see menu");

            String input = scanner.next();

            switch (input){

                case "menu":
                    managerMenu.menu();
                    break;

                case "add":
                    messageCatalog = addMessage.add(messageCatalog,scanner);
                    showMessage.show(messageCatalog);
                    break;

                case "simple":
                    showMessage.simple();
                    showMessage.show(messageCatalog);
                    break;

                case "full":
                    showMessage.full();
                    showMessage.show(messageCatalog);
                    break;

                case "delete":
                    messageCatalog = messageOptions.deleteManager(scanner,messageCatalog);
                    showMessage.show(messageCatalog);
                    break;

                case "end":
                    end = false;
                    break;

                default:
                    showMessage.show(filterMessage.filer(messageCatalog,input));
                    break;

            }

        }while (end);
    }
}
