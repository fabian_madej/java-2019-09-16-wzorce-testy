package fabian.task.messageManager;

import fabian.task.message.Message;
import fabian.task.message.MessageCatalog;

import java.util.Collection;

public class FilterMessage {
    public MessageCatalog filer(MessageCatalog messageCatalog, String input) {

        MessageCatalog filthMessageCatalog = new MessageCatalog();
        Collection<Message> messages = messageCatalog.getMessages();

        for(Message message : messages){
            if(message.getTitle().contains(input)){
                filthMessageCatalog.addMessage(message);
            }else if(message.getAuthor().contains(input)){
                filthMessageCatalog.addMessage(message);
            }else if(message.getContents().contains(input)){
                filthMessageCatalog.addMessage(message);
            }else if(String.valueOf(message.getCreation_date()).contains(input)){
                filthMessageCatalog.addMessage(message);
            }
        }

        return filthMessageCatalog;
    }
}
