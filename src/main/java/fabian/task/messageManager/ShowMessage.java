package fabian.task.messageManager;

import fabian.task.message.Message;
import fabian.task.message.MessageCatalog;

import java.util.Collection;

public class ShowMessage {

    private boolean isSimple = true;


    public void show(MessageCatalog messageCatalog) {
        Collection<Message> messages = messageCatalog.getMessages();

        if(isSimple){
            for(Message message : messages){
                System.out.println(message.toSimpleString());
            }
        }else {
            for(Message message : messages){
                System.out.println(message);
            }
        }
    }

    public void simple() {
        isSimple=true;
    }

    public void full() {
        isSimple=false;
    }
}
