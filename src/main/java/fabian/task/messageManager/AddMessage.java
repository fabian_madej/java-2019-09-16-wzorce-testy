package fabian.task.messageManager;

import fabian.task.message.Message;
import fabian.task.message.MessageBuilder;
import fabian.task.message.MessageCatalog;

import java.time.LocalDate;
import java.util.Scanner;

public class AddMessage {
    public MessageCatalog add(MessageCatalog messageCatalog, Scanner scanner) {

        MessageBuilder messageBuilder = new MessageBuilder();
        boolean end = true;

        do {

            System.out.println("Who are you?\nGive me your name: ");
            messageBuilder.withAuthor(scanner.next());

            System.out.println("Title: ");
            messageBuilder.withTitle(scanner.next());

            System.out.println("Write content of message: ");
            messageBuilder.withContents(scanner.next());

            messageBuilder.withCreation_date(LocalDate.now());

            Message message = messageBuilder.build();
            System.out.println(message);
            System.out.println("Write 'save' to save this message");
            System.out.println("Write anything to start again");
            System.out.println("Write 'back' to don't save");

            String input =scanner.next();

            if(input.equals("save")){
                messageCatalog.addMessage(message);
                end=false;
            }else if(input.equals("back")){
                end=false;
            }

        }while (end);

        return messageCatalog;
    }
}
