package fabian.task.messageManager;

import fabian.task.message.Message;
import fabian.task.message.MessageCatalog;

import java.util.Collection;

public class DeleteMessage {
    public MessageCatalog delete(MessageCatalog messageCatalog, MessageCatalog filthMessageCatalog) {

        Collection<Message> filthMessage =filthMessageCatalog.getMessages();

        if(filthMessage.isEmpty()){
            return messageCatalog;
        }else {
            for (Message message : filthMessage) {
                messageCatalog.removeMessage(message);
            }
            return messageCatalog;
        }
    }
}
