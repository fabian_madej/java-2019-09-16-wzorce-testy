package fabian.task.messageManager;

import fabian.task.message.MessageBuilder;
import fabian.task.message.MessageCatalog;
import org.junit.Test;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.*;

public class FilterMessageTest {

    @Test
    public void filterTestWhenMessageContainsInput(){

        String input = "fdasfads";
        MessageCatalog messageCatalog = new MessageCatalog();
        FilterMessage filterMessage = new FilterMessage();
        MessageBuilder messageBuilder = new MessageBuilder();
        messageBuilder.withTitle("afsd")
                .withAuthor("adsf")
                .withContents("fdasfads")
                .withCreation_date(LocalDate.now());
        messageCatalog.addMessage(messageBuilder.build());

        MessageCatalog filthMessageCatalog = filterMessage.filer(messageCatalog,input);

        assertThat(filthMessageCatalog.getMessages()).isEqualTo(messageCatalog.getMessages());
    }

    @Test
    public void filterTestWhenMessageDontContainsInput(){
        String input = "xxxx";
        MessageCatalog messageCatalog = new MessageCatalog();
        FilterMessage filterMessage = new FilterMessage();
        MessageBuilder messageBuilder = new MessageBuilder();
        messageBuilder.withTitle("afsd")
                .withAuthor("adsf")
                .withContents("fdasfads")
                .withCreation_date(LocalDate.now());
        messageCatalog.addMessage(messageBuilder.build());

        MessageCatalog filthMessageCatalog = filterMessage.filer(messageCatalog,input);

        assertThat(filthMessageCatalog.getMessages()).isEmpty();
    }
}
