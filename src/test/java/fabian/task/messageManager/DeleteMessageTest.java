package fabian.task.messageManager;

import fabian.task.message.Message;
import fabian.task.message.MessageBuilder;
import fabian.task.message.MessageCatalog;
import org.junit.Test;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.*;

public class DeleteMessageTest {

    @Test
    public void deleteTestIfFilthMessageCatalogISNotNULL(){
        MessageCatalog filthMessageCatalog = new MessageCatalog();
        MessageCatalog messageCatalog = new MessageCatalog();
        MessageBuilder messageBuilder = new MessageBuilder();
        messageBuilder.withTitle("afsd")
                .withAuthor("adsf")
                .withContents("fdasfads")
                .withCreation_date(LocalDate.now());
        Message message = messageBuilder.build();
        messageCatalog.addMessage(message);
        filthMessageCatalog.addMessage(message);
        DeleteMessage deleteMessage = new DeleteMessage();

        messageCatalog = deleteMessage.delete(messageCatalog,filthMessageCatalog);

        assertThat(messageCatalog.getMessages()).isEmpty();
    }

    @Test
    public void deleteTestIfFilthMessageCatalogISNULL(){
        MessageCatalog filthMessageCatalog = new MessageCatalog();
        MessageCatalog messageCatalog = new MessageCatalog();
        MessageBuilder messageBuilder = new MessageBuilder();
        messageBuilder.withTitle("afsd")
                .withAuthor("adsf")
                .withContents("fdasfads")
                .withCreation_date(LocalDate.now());
        Message message = messageBuilder.build();
        messageCatalog.addMessage(message);
        DeleteMessage deleteMessage = new DeleteMessage();

        messageCatalog = deleteMessage.delete(messageCatalog,filthMessageCatalog);

        assertThat(messageCatalog.getMessages()).isNotEmpty();
    }
}
