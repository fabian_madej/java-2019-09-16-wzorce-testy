package fabian.task.message;

import org.junit.Test;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.*;

public class MessageBuilderTest {

    @Test
    public void buildTest(){
        MessageBuilder messageBuilder = new MessageBuilder();
        messageBuilder
                .withTitle("sdasd")
                .withAuthor("sad")
                .withContents("ads")
                .withCreation_date(LocalDate.of(2019, 11, 11));

        Message message = messageBuilder.build();

        assertThat(message).isNotNull();
    }

    @Test
    public void withTitleTest(){
        MessageBuilder messageBuilder = new MessageBuilder();
        messageBuilder
                .withTitle("sdasd")
                .withAuthor("sad")
                .withContents("ads")
                .withCreation_date(LocalDate.of(2019, 11, 11));

        Message message = messageBuilder.build();

        assertThat(message.getTitle()).isNotNull();
    }

    @Test
    public void withAuthorTest(){
        MessageBuilder messageBuilder = new MessageBuilder();
        messageBuilder
                .withTitle("sdasd")
                .withAuthor("sad")
                .withContents("ads")
                .withCreation_date(LocalDate.of(2019, 11, 11));

        Message message = messageBuilder.build();

        assertThat(message.getAuthor()).isNotNull();
    }

    @Test
    public void withContentsTest(){
        MessageBuilder messageBuilder = new MessageBuilder();
        messageBuilder
                .withTitle("sdasd")
                .withAuthor("sad")
                .withContents("ads")
                .withCreation_date(LocalDate.of(2019, 11, 11));

        Message message = messageBuilder.build();

        assertThat(message.getContents()).isNotNull();
    }

    @Test
    public void withCreation_dateTest(){
        MessageBuilder messageBuilder = new MessageBuilder();
        messageBuilder
                .withTitle("sdasd")
                .withAuthor("sad")
                .withContents("ads")
                .withCreation_date(LocalDate.now());

        Message message = messageBuilder.build();

        assertThat(message.getCreation_date()).isNotNull();
    }

}
