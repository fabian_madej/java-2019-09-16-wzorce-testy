package fabian.task.message;

import org.junit.Test;
import static org.assertj.core.api.Assertions.*;

import java.time.LocalDate;

public class MessageCatalogTest {

    @Test
    public void addMessageTest(){

        MessageCatalog messageCatalog = new MessageCatalog();
        MessageBuilder messageBuilder = new MessageBuilder();
        messageBuilder
                .withTitle("sdasd")
                .withAuthor("sad")
                .withContents("ads")
                .withCreation_date(LocalDate.of(2019, 11, 11));
        Message message = messageBuilder.build();

        messageCatalog.addMessage(message);

        assertThat(messageCatalog.getMessages()).isNotEmpty();
    }

    @Test
    public void removeMessageTest(){
        MessageCatalog messageCatalog = new MessageCatalog();
        MessageBuilder messageBuilder = new MessageBuilder();
        messageBuilder
                .withTitle("sdasd")
                .withAuthor("sad")
                .withContents("ads")
                .withCreation_date(LocalDate.of(2019, 11, 11));
        Message message = messageBuilder.build();
        messageCatalog.addMessage(message);

        messageCatalog.removeMessage(message);

        assertThat(messageCatalog.getMessages()).isEmpty();
    }

}
